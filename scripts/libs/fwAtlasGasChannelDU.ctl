// We are not using the value changed function. Implement the functionality here
// with a callback function.
// Note: We assume standard FSM conventions: DU FSM node name = DP name of device

#uses "fwAtlasGas/fwAtlasGasFsm.ctl"

fwAtlasGasChannelDU_initialize(string domain, string device)
{
  
  fwAtlasGasFsm_setupDuDpConnect(domain, device);

}

fwAtlasGasChannelDU_valueChanged( string domain, string device, string &fwState )
{
}


#uses "fwAtlasGas/fwAtlasGasFsm.ctl"

fwAtlasGasChannelDU_doCommand(string domain, string device, string command)
{
	if (command == "REFRESH")
	{
    fwAtlasGasFsm_refreshDU(domain, device);
	}
	if (command == "MASK_BAD")
	{
   string state;
   fwDU_getState(domain, device, state);
   fwDU_startTimeout(0, domain, device, state, "");                                    
		dpSet(device+".DataStatus.ignoreDipBad",TRUE);
	}
	if (command == "UNMASK_BAD")
	{
   string state;
   fwDU_getState(domain, device, state);
   fwDU_startTimeout(0, domain, device, state, "");      
		dpSet(device+".DataStatus.ignoreDipBad",FALSE);
	}
	if (command == "DECLARE_CLOSED")
	{
		dpSet(device+".IsolationValve",FALSE);
	}
	if (command == "DECLARE_OPEN")
	{
		dpSet(device+".IsolationValve",TRUE);
	}
}


