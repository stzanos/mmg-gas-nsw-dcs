//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)

  "mmgGas_constants.ctl" is a library that includes all constants of the project"
*/

#uses "mmgGas/mmgGas_constants.ctl"


// Default MDT operations' privilege

string mmgGas_getPrivilege_DcsOperation() {

  return "MMG:Control";
}

// Privilege for DCS Expert actions

string mmgGas_getPrivilege_DcsExpert() {

  return "MMG:Expert";
}


// MMG Admin privilege

string mmgGas_getPrivilege_Admin() {

  return "MMG:Administration";

}


// MMG detector, non DCS expert. Expert in all hardware but not DCS code stuff.

string mmgGas_getPrivilege_DetExpert() {

  return "MMGDET:Expert";
}

/*
// MMG gas expert, not necessarily a detector expert.

string mmgGas_getPrivilege_GasExpert() {

  return "MMGGAS:Expert";
}

// MDT alignment expert.

string mmgGas_getPrivilege_AlignExpert() {

  return "MMGALGN:Expert";
}*/

/*
///////////////////////////////////////////////////////////////////////////////////
// Access control panel protection: Generic function, to be set up in panel initialize
// script with fwAccessControl_setupPanel("ApplyPanelAccessControl", exceptionInfo);
// One can define several privileges which would grant access to panel shapes
////////////////////////////////////////////////////////////////////////////////////

dyn_string _mdtAC_priv2Grant;
dyn_string _mdtAC_shapes2Protect;

void mdtAC_ApplyPanelAccessControl(string s1, string s2) {

  dyn_string exceptionInfo;
  dyn_string allDomains, allDomainsFull;
  string privilege, dom;
  dyn_string comp;
  bool granted = FALSE;
  bool isExpert = FALSE;

  dyn_string shapeList = _mdtAC_shapes2Protect;

  fwAccessControl_getAllDomains(allDomains, allDomainsFull, exceptionInfo);

  for (int i = 1; i <= dynlen(_mdtAC_priv2Grant); i++) {
    privilege = _mdtAC_priv2Grant[i];
    comp = strsplit(privilege, ":");
    dom = comp[1];
    if (dynContains(allDomains, dom))               // check if specified access control domain exists
      fwAccessControl_isGranted(privilege, granted, exceptionInfo);
    else dynAppend(exceptionInfo, makeDynString("Domain " + dom + " not found"));
  //if (dynlen(exceptionInfo) > 0) fwAccessControl_displayException(exceptionInfo);
    if (granted) {
      isExpert = TRUE;
      break;
    }
  }

  if (dynlen(exceptionInfo))
    DebugN("WARNING: Panel Access control, problem checking AC rights", exceptionInfo);

  if (!isExpert) {
    for (int i = 1; i <= dynlen(_mdtAC_shapes2Protect); i++) {
      setValue(_mdtAC_shapes2Protect[i], "enabled", FALSE);
      setValue(_mdtAC_shapes2Protect[i], "toolTipText", "Sorry, you don't have privileges for this operation");
    }
  }
  else if (isExpert) {
    for (int i = 1; i <= dynlen(_mdtAC_shapes2Protect); i++) {
      setValue(_mdtAC_shapes2Protect[i], "enabled", TRUE);
      setValue(_mdtAC_shapes2Protect[i], "toolTipText", "");
    }
  }

}

///////////////////////////////////////////////////////////////////////////////////////////
// Check if user has one or more of the specified access control privileges
// return: TRUE on successful function execution, false on any error
///////////////////////////////////////////////////////////////////////////////////////////


bool mdtAC_checkPrivileges(dyn_string privileges, bool &privGranted, bool showPopup = FALSE) {

  dyn_string exceptionInfo;
  dyn_string allDomains, allDomainsFull;
  string privilege, dom;
  dyn_string comp;
  bool granted = FALSE;
  bool ret = TRUE;

  fwAccessControl_getAllDomains(allDomains, allDomainsFull, exceptionInfo);

  for (int i = 1; i <= dynlen(privileges); i++) {
    comp = strsplit(privileges[i], ":");
    dom = comp[1];
    if (dynContains(allDomains, dom))               // check if specified access control domain exists
      fwAccessControl_isGranted(privileges[i], granted, exceptionInfo);
    else dynAppend(exceptionInfo, makeDynString("Domain " + dom + " not found"));
  //if (dynlen(exceptionInfo) > 0) fwAccessControl_displayException(exceptionInfo);
    if (granted) break;
  }

  if (dynlen(exceptionInfo)) {
    DebugN("WARNING: Panel Access control, problem checking AC rights", exceptionInfo);
    ret = FALSE;
  }

  if (showPopup && !granted) {
    ChildPanelOn("vision/MessageInfo1", "Information",
                 makeDynString("Sorry, you don't have the privileges for this operation!"), 100, 100);
  }

  privGranted = granted;
  return ret;

}
*/




