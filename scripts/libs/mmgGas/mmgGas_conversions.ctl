//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)

  "mmgGas_conversions.ctl" is a library to be used to make conversions between
  values that are commonly used or mapped in the project."
*/




string mmgGas_intToString(int value) {

  if(value > 9) return (string)value;
  else return (string)("0" + value);
}

int mmgGas_sideToRack(string side) {

  if(side == "A") return 61;
  else return 62;
}

string mmgGas_rackToSide(int rack) {

  if(rack == 61) return "A";
  else return "C";
}
