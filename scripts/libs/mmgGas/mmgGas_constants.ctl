//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)

  "mmgGas_constants.ctl" is a library that includes all constants of the project"
*/

const float LDW_VOL = 26; // In L
const float SDW_VOL = 20; // In L

string mmgGas_getGasSystemName() {

  return "ATLMUONSWINF1:";
}
