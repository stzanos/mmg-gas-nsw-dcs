//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)

  "mmgGas_infrastructure.ctl" is a library to be used to create the infrastructure of
  the MicroMegas Gas project.
*/


void mmgGas_createComplexDptChain() {

  int i, j;
  dyn_dyn_string msdpt;
  dyn_dyn_int tmsdpt;

  int n;
  dyn_errClass err;

  // Creating the data type

  msdpt[i=1] = makeDynString("fwAtlasGasChain", "", "");

    msdpt[i=i+1] = makeDynString("", "ExternalTrigger", "");
    msdpt[i=i+1] = makeDynString("", "ExternallySelectedSource", "");
    msdpt[i=i+1] = makeDynString("", "IhibitFlag", "");
    msdpt[i=i+1] = makeDynString("", "OptionMode", "");
    msdpt[i=i+1] = makeDynString("", "SelectedSource", "");
    msdpt[i=i+1] = makeDynString("", "State", "");

    msdpt[i=i+1] = makeDynString("", "quality", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "invalid");

    msdpt[i=i+1] = makeDynString("", "DataStatus", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "unknown");
      msdpt[i=i+1] = makeDynString("", "", "ignoreDipBad");

    msdpt[i=i+1] = makeDynString("", "UserDefined", "");

  // ---------------------------------------------------------- //

  tmsdpt[j=1] = makeDynInt(DPEL_STRUCT);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_BOOL);  // ExternalTrigger
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_INT);  // ExternallySelectedSource
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_BOOL);  // InhibitFlag
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_INT);  // OptionMode
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_INT);  // SelectedSource
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_INT);  // State

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // quality

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // DataStatus

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // UserDefined

  n = dpTypeCreate(msdpt, tmsdpt);
  DebugN("Creating fwAtlasGasChain datapoint type for MMG. Result: ", n);

  if(n == 0) {
    DebugN("Einai fovero e?");
  }

  err = getLastError();
  if(dynlen(err) > 0) {

    errorDialog(err);
    throwError(err);
    DebugN("De douleuei tipota.");
  }
  else {

    DebugN("Den yparxei kanena provlima mikre.");
    DebugN(" -------- ");
  }
}

void mmgGas_createComplexDptChannel() {

  int i, j;
  dyn_dyn_string msdpt;
  dyn_dyn_int tmsdpt;

  int n;
  dyn_errClass err;

  // Creating the data type

  msdpt[i=1] = makeDynString("fwAtlasGasChannel", "", "");

    msdpt[i=i+1] = makeDynString("", "InFlow", "");
    msdpt[i=i+1] = makeDynString("", "IsolationValve", "");
    msdpt[i=i+1] = makeDynString("", "OutFlow", "");
    msdpt[i=i+1] = makeDynString("", "Pressure", "");

    msdpt[i=i+1] = makeDynString("", "quality", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "invalid");

    msdpt[i=i+1] = makeDynString("", "DataStatus", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "unknown");
      msdpt[i=i+1] = makeDynString("", "", "ignoreDipBad");

    msdpt[i=i+1] = makeDynString("", "UserDefined", "");

      msdpt[i=i+1] = makeDynString("", "", "ActiveVolume");
      msdpt[i=i+1] = makeDynString("", "", "interlockHv");
      msdpt[i=i+1] = makeDynString("", "", "GasLoss");
      msdpt[i=i+1] = makeDynString("", "", "GasLossFrac");

  // ---------------------------------------------------------- //

  tmsdpt[j=1] = makeDynInt(DPEL_STRUCT);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT);  // inFlow
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_BOOL);  // IsolationValve
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT);  // outFlow
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT);  // Pressure

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // quality

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // DataStatus

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // UserDefined

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_FLOAT); // ActiveVolume
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL); // interlockHv
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_FLOAT); // GasLoss
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_FLOAT); // GasLossFrac

  n = dpTypeCreate(msdpt, tmsdpt);
  DebugN("Creating fwAtlasGasChannel datapoint type for MMG. Result: ", n);

  if(n == 0) {
    DebugN("Einai fovero e?");
  }

  err = getLastError();
  if(dynlen(err) > 0) {

    errorDialog(err);
    throwError(err);
    DebugN("De douleuei tipota.");
  }
  else {
    DebugN("Den yparxei kanena provlima mikre.");
    DebugN(" -------- ");
  }
}

void mmgGas_createComplexDptDistribution() {

  int i, j;
  dyn_dyn_string msdpt;
  dyn_dyn_int tmsdpt;

  int n;
  dyn_errClass err;

  // Creating the data type

  msdpt[i=1] = makeDynString("fwAtlasGasDistribution", "", "");

    msdpt[i=i+1] = makeDynString("", "State", "");
    msdpt[i=i+1] = makeDynString("", "TotalInputFlow", "");

    msdpt[i=i+1] = makeDynString("", "quality", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "invalid");

    msdpt[i=i+1] = makeDynString("", "DataStatus", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "unknown");
      msdpt[i=i+1] = makeDynString("", "", "ignoreDipBad");

    msdpt[i=i+1] = makeDynString("", "UserDefined", "");

      msdpt[i=i+1] = makeDynString("", "", "interlockHv");
      msdpt[i=i+1] = makeDynString("", "", "GasLoss");

  // ---------------------------------------------------------- //

  tmsdpt[j=1] = makeDynInt(DPEL_STRUCT);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_INT);
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // quality

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // DataStatus

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // UserDefined

  n = dpTypeCreate(msdpt, tmsdpt);
  DebugN("Creating fwAtlasGasDistribution datapoint type for MMG gas. Result: ", n);

  if(n == 0) {
    DebugN("Etsi bravo.");
  }

  err = getLastError();
  if(dynlen(err) > 0) {

    errorDialog(err);
    throwError(err);
    DebugN("De douleuei tipota.");
  }
  else {

    DebugN("Den yparxei kanena provlima mikre.");
    DebugN(" -------- ");
  }
}

void mmgGas_createComplexDptGasSystem() {

  int i, j;
  dyn_dyn_string msdpt;
  dyn_dyn_int tmsdpt;

  int n;
  dyn_errClass err;

  // Creating the data type

  msdpt[i=1] = makeDynString("fwAtlasGasGasSystem", "", "");

    msdpt[i=i+1] = makeDynString("", "State", "");
    msdpt[i=i+1] = makeDynString("", "Status", "");

    msdpt[i=i+1] = makeDynString("", "quality", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "invalid");

    msdpt[i=i+1] = makeDynString("", "DataStatus", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "unknown");
      msdpt[i=i+1] = makeDynString("", "", "ignoreDipBad");

    msdpt[i=i+1] = makeDynString("", "UserDefined", "");

  // ---------------------------------------------------------- //

  tmsdpt[j=1] = makeDynInt(DPEL_STRUCT);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_INT); // state
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_BOOL); // status

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // quality

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // DataStatus

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // UserDefined

  n = dpTypeCreate(msdpt, tmsdpt);
  DebugN("Creating fwAtlasGasGasSystem datapoint type. Result: ", n);

  if(n == 0) {
    DebugN("To kefi mas kanoume.");
  }

  err = getLastError();
  if(dynlen(err) > 0) {

    errorDialog(err);
    throwError(err);
    DebugN("De douleuei tipota.");
  }
  else {

    DebugN("Den yparxei kanena provlima mikre.");
    DebugN(" -------- ");
  }
}

void mmgGas_createComplexDptMixer() {

  int i, j;
  dyn_dyn_string msdpt;
  dyn_dyn_int tmsdpt;

  int n;
  dyn_errClass err;

  // Creating the data type

  msdpt[i=1] = makeDynString("fwAtlasGasMixer", "", "");

    msdpt[i=i+1] = makeDynString("", "Interlock", "");
    msdpt[i=i+1] = makeDynString("", "State", "");
    msdpt[i=i+1] = makeDynString("", "OutPressure", "");
    msdpt[i=i+1] = makeDynString("", "TotalFlow", "");
    msdpt[i=i+1] = makeDynString("", "TotalVol", "");
    msdpt[i=i+1] = makeDynString("", "Line1InputPressure", "");
    msdpt[i=i+1] = makeDynString("", "Line2InputPressure", "");
    msdpt[i=i+1] = makeDynString("", "Line3InputPressure", "");
    msdpt[i=i+1] = makeDynString("", "Line1LFmfcFeedback", "");
    msdpt[i=i+1] = makeDynString("", "Line2LFmfcFeedback", "");
    msdpt[i=i+1] = makeDynString("", "Line3LFmfcFeedback", "");
    msdpt[i=i+1] = makeDynString("", "Line1Ratio", "");
    msdpt[i=i+1] = makeDynString("", "Line2Ratio", "");
    msdpt[i=i+1] = makeDynString("", "Line3Ratio", "");
    msdpt[i=i+1] = makeDynString("", "Line1TotRunningTime", "");
    msdpt[i=i+1] = makeDynString("", "Line2TotRunningTime", "");
    msdpt[i=i+1] = makeDynString("", "Line3TotRunningTime", "");

    msdpt[i=i+1] = makeDynString("", "quality", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "invalid");

    msdpt[i=i+1] = makeDynString("", "DataStatus", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "unknown");
      msdpt[i=i+1] = makeDynString("", "", "ignoreDipBad");

    msdpt[i=i+1] = makeDynString("", "UserDefined", "");

  // ---------------------------------------------------------- //

  tmsdpt[j=1] = makeDynInt(DPEL_STRUCT);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_BOOL); // Interlock
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_INT); // State
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // OutPressure
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // TotalFlow
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // TotalVol
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // LineInputPressure
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT);
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT);
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // LineLFmfcFeedback
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT);
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT);
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // LineRatio
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT);
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT);
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // LineTotRunningTime
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT);
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // quality

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // DataStatus

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // UserDefined

  n = dpTypeCreate(msdpt, tmsdpt);
  DebugN("Creating fwAtlasGasMixer datapoint type for MMG gas. Result: ", n);

  if(n == 0) {
    DebugN("Einai fovero e??");
  }

  err = getLastError();
  if(dynlen(err) > 0) {

    errorDialog(err);
    throwError(err);
    DebugN("De douleuei tipota.");
  }
  else {

    DebugN("Den yparxei kanena provlima mikre.");
    DebugN(" -------- ");
  }
}

void mmgGas_createComplexDptPlcCounter() {

  int i, j;
  dyn_dyn_string msdpt;
  dyn_dyn_int tmsdpt;

  int n;
  dyn_errClass err;

  // Creating the data type

  msdpt[i=1] = makeDynString("fwAtlasGasPlcCounter", "", "");

    msdpt[i=i+1] = makeDynString("", "AliveCounter", "");

    msdpt[i=i+1] = makeDynString("", "quality", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "invalid");

    msdpt[i=i+1] = makeDynString("", "DataStatus", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "unknown");
      msdpt[i=i+1] = makeDynString("", "", "ignoreDipBad");

    msdpt[i=i+1] = makeDynString("", "UserDefined", "");

      msdpt[i=i+1] = makeDynString("", "", "interlockHv");

  // ---------------------------------------------------------- //

  tmsdpt[j=1] = makeDynInt(DPEL_STRUCT);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_INT); // AliveCounter

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // quality

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // DataStatus

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // UserDefined

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL); // interlockHv

  n = dpTypeCreate(msdpt, tmsdpt);
  DebugN("Creating fwAtlasGasPlcCounter datapoint type for MMG gas. Result: ", n);

  if(n == 0) {
    DebugN("Einai fovero.");
  }

  err = getLastError();
  if(dynlen(err) > 0) {

    errorDialog(err);
    throwError(err);
    DebugN("De douleuei tipota.");
  }
  else {

    DebugN("Den yparxei kanena provlima mikre.");
    DebugN(" -------- ");
  }
}

void mmgGas_createComplexDptRack() {

  int i, j;
  dyn_dyn_string msdpt;
  dyn_dyn_int tmsdpt;

  int n;
  dyn_errClass err;

  // Creating the data type

  msdpt[i=1] = makeDynString("fwAtlasGasRack", "", "");

    msdpt[i=i+1] = makeDynString("", "ChPressure", "");
    msdpt[i=i+1] = makeDynString("", "InPressure", "");
    msdpt[i=i+1] = makeDynString("", "RegPressure", "");
    msdpt[i=i+1] = makeDynString("", "State", "");

    msdpt[i=i+1] = makeDynString("", "quality", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "invalid");

    msdpt[i=i+1] = makeDynString("", "DataStatus", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "unknown");
      msdpt[i=i+1] = makeDynString("", "", "ignoreDipBad");

    msdpt[i=i+1] = makeDynString("", "UserDefined", "");

  // ---------------------------------------------------------- //

  tmsdpt[j=1] = makeDynInt(DPEL_STRUCT);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // ChPressure
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // InPressure
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // RegPressure
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_INT); // State

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // quality

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // DataStatus

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // UserDefined


  n = dpTypeCreate(msdpt, tmsdpt);
  DebugN("Creating fwAtlasGasRack datapoint type for MMG gas. Result: ", n);

  if(n == 0) {
    DebugN("Telika ena koubi itan mono.");
  }

  err = getLastError();
  if(dynlen(err) > 0) {

    errorDialog(err);
    throwError(err);
    DebugN("De douleuei tipota.");
  }
  else {

    DebugN("Den yparxei kanena provlima mikre.");
    DebugN(" -------- ");
  }
}

void mmgGas_createComplexDptSource() {

  int i, j;
  dyn_dyn_string msdpt;
  dyn_dyn_int tmsdpt;

  int n;
  dyn_errClass err;

  // Creating the data type

  msdpt[i=1] = makeDynString("fwAtlasGasSource", "", "");

    msdpt[i=i+1] = makeDynString("", "CO2", "");
    msdpt[i=i+1] = makeDynString("", "H2O", "");
    msdpt[i=i+1] = makeDynString("", "IR", "");
    msdpt[i=i+1] = makeDynString("", "O2", "");

    msdpt[i=i+1] = makeDynString("", "quality", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "invalid");

    msdpt[i=i+1] = makeDynString("", "DataStatus", "");

      msdpt[i=i+1] = makeDynString("", "", "uncertain");
      msdpt[i=i+1] = makeDynString("", "", "bad");
      msdpt[i=i+1] = makeDynString("", "", "unknown");
      msdpt[i=i+1] = makeDynString("", "", "ignoreDipBad");

    msdpt[i=i+1] = makeDynString("", "UserDefined", "");

  // ---------------------------------------------------------- //

  tmsdpt[j=1] = makeDynInt(DPEL_STRUCT);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // CO2
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // H2O
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // IR
    tmsdpt[j=j+1] = makeDynInt(0, DPEL_FLOAT); // O2

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // quality

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // DataStatus

      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);
      tmsdpt[j=j+1] = makeDynInt(0, 0, DPEL_BOOL);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRUCT); // UserDefined


  n = dpTypeCreate(msdpt, tmsdpt);
  DebugN("Creating fwAtlasGasSource datapoint type for MMG gas. Result: ", n);

  if(n == 0) {
    DebugN("Telika ena koubi itan mono.");
  }

  err = getLastError();
  if(dynlen(err) > 0) {

    errorDialog(err);
    throwError(err);
    DebugN("De douleuei tipota.");
  }
  else {

    DebugN("Den yparxei kanena provlima mikre.");
    DebugN(" -------- ");
  }
}

void mmgGas_deleteComplexDpts() {

  dyn_string dpts = makeDynString("Chain", "Channel", "Distribution", "GasSystem", "Mixer", "PlcCounter", "Rack", "Source");

  for(int i = 1; i <= dynlen(dpts); i++) {

    if(0 != dpTypeDelete("fwAtlasGas" + dpts[i]))
      error("Cannot delete fwAtlasGas" + dpts[i] + ".");
  }
}

void mmgGas_complexDatapointsCreate() {

  dyn_int rack = makeDynInt(61, 62);
  string mmgGas = "gas_MMG_";

  for(int chain = 1; chain <= 2; chain++) {
    if(0 != dpCreate(mmgGas + "Analysis_Chain" + chain, "fwAtlasGasChain"))
      error(mmgGas + "Analysis_Chain" + chain + " datapoints were not created successfully.");
    if(chain == 1) {
      for(int k = 141; k <= 147; k++) {
        if(0 != dpCreate(mmgGas + "Analysis_Chain" + chain + "_Source" + k, "fwAtlasGasSource"))
          error(mmgGas + "Analysis_Chain" + chain + "_Source" + k + " datapoints were not created successfully.");
      }
    }
    else {
      if(0 != dpCreate(mmgGas + "Analysis_Chain" + chain + "_Source141", "fwAtlasGasSource"))
        error(mmgGas + "Analysis_Chain" + chain + "_Source141 datapoints were not created successfully.");
      if(0 != dpCreate(mmgGas + "Analysis_Chain" + chain + "_Source142", "fwAtlasGasSource"))
        error(mmgGas + "Analysis_Chain" + chain + "_Source142 datapoints were not created successfully.");
      if(0 != dpCreate(mmgGas + "Analysis_Chain" + chain + "_Source181", "fwAtlasGasSource"))
        error(mmgGas + "Analysis_Chain" + chain + "_Source181 datapoints were not created successfully.");
      if(0 != dpCreate(mmgGas + "Analysis_Chain" + chain + "_Source182", "fwAtlasGasSource"))
        error(mmgGas + "Analysis_Chain" + chain + "_Source182 datapoints were not created successfully.");
      if(0 != dpCreate(mmgGas + "Analysis_Chain" + chain + "_Source183", "fwAtlasGasSource"))
        error(mmgGas + "Analysis_Chain" + chain + "_Source183 datapoints were not created successfully.");
    }
  }
  if(0 != dpCreate("template_fwAtlasGasChain", "fwAtlasGasChain"))
    error(mmgGas + "Analysis_Chain" + chain + "_Source183 datapoints were not created successfully.");
  if(0 != dpCreate("template_fwAtlasGasSource", "fwAtlasGasSource"))
    error(mmgGas + "Analysis_Chain" + chain + "_Source183 datapoints were not created successfully.");

  for(int i = 1; i <= dynlen(rack); i++) {
    if(0 != dpCreate(mmgGas + "Distribution_Rack" + rack[i], "fwAtlasGasRack"))
      error(mmgGas + "Distribution_Rack" + rack[i] + " datapoints were not created successfully.");
    for(int chan = 1; chan <= 16 ; chan++) {
      if(0 != dpCreate(mmgGas + "Distribution_Rack" + rack[i] + "_Channel" + chan, "fwAtlasGasChannel"))
        error(mmgGas + "Distribution_Rack" + rack[i] + "_Channel" + chan + " datapoints were not created successfully.");
    }
  }
  if(0 != dpCreate("template_fwAtlasGasRack", "fwAtlasGasRack"))
    error("template_fwAtlasGasRack datapoints were not created successfully.");
  if(0 != dpCreate("template_fwAtlasGasChannel", "fwAtlasGasChannel"))
    error("template_fwAtlasGasChannel datapoints were not created successfully.");

  if(0 != dpCreate(mmgGas + "Distribution", "fwAtlasGasDistribution"))
    error(mmgGas + "Distribution datapoints were not created successfully.");
  if(0 != dpCreate("template_fwAtlasGasDistribution", "fwAtlasGasDistribution"))
    error("template_fwAtlasGasDistribution datapoints were not created successfully.");

  if(0 != dpCreate(mmgGas + "GasSystem", "fwAtlasGasGasSystem"))
    error(mmgGas + "GasSystem datapoints were not created successfully.");
  if(0 != dpCreate("template_fwAtlasGasGasSystem", "fwAtlasGasGasSystem"))
    error("template_fwAtlasGasGasSystem datapoints were not created successfully.");

  if(0 != dpCreate(mmgGas + "Mixer", "fwAtlasGasMixer"))
    error(mmgGas + "Mixer datapoints were not created successfully.");
  if(0 != dpCreate("template_fwAtlasGasMixer", "fwAtlasGasMixer"))
    error("template_fwAtlasGasMixer datapoints were not created successfully.");

  if(0 != dpCreate(mmgGas + "PlcCounter", "fwAtlasGasPlcCounter"))
    error(mmgGas + "PlcCounter datapoints were not created successfully.");
  if(0 != dpCreate("template_fwAtlasGasPlcCounter", "fwAtlasGasPlcCounter"))
    error("template_fwAtlasGasPlcCounter datapoints were not created successfully.");
}

void mmgGas_complexDatapointsDelete() {

  dyn_int rack = makeDynInt(61, 62);
  string mmgGas = "gas_MMG_";

  for(int chain = 1; chain <= 2; chain++) {
    if(0 != dpDelete(mmgGas + "Analysis_Chain" + chain, "fwAtlasGasChain"))
      error(mmgGas + "Analysis_Chain" + chain + " datapoints were not Deleted successfully.");
    if(chain == 1) {
      for(int k = 141; k <= 147; k++) {
        if(0 != dpDelete(mmgGas + "Analysis_Chain" + chain + "_Source" + k, "fwAtlasGasSource"))
          error(mmgGas + "Analysis_Chain" + chain + "_Source" + k + " datapoints were not Deleted successfully.");
      }
    }
    else {
      if(0 != dpDelete(mmgGas + "Analysis_Chain" + chain + "_Source141", "fwAtlasGasSource"))
        error(mmgGas + "Analysis_Chain" + chain + "_Source141 datapoints were not Deleted successfully.");
      if(0 != dpDelete(mmgGas + "Analysis_Chain" + chain + "_Source142", "fwAtlasGasSource"))
        error(mmgGas + "Analysis_Chain" + chain + "_Source142 datapoints were not Deleted successfully.");
      if(0 != dpDelete(mmgGas + "Analysis_Chain" + chain + "_Source181", "fwAtlasGasSource"))
        error(mmgGas + "Analysis_Chain" + chain + "_Source181 datapoints were not Deleted successfully.");
      if(0 != dpDelete(mmgGas + "Analysis_Chain" + chain + "_Source182", "fwAtlasGasSource"))
        error(mmgGas + "Analysis_Chain" + chain + "_Source182 datapoints were not Deleted successfully.");
      if(0 != dpDelete(mmgGas + "Analysis_Chain" + chain + "_Source183", "fwAtlasGasSource"))
        error(mmgGas + "Analysis_Chain" + chain + "_Source183 datapoints were not Deleted successfully.");
    }
  }
  if(0 != dpDelete("template_fwAtlasGasChain", "fwAtlasGasChain"))
    error(mmgGas + "Analysis_Chain" + chain + "_Source183 datapoints were not Deleted successfully.");
  if(0 != dpDelete("template_fwAtlasGasSource", "fwAtlasGasSource"))
    error(mmgGas + "Analysis_Chain" + chain + "_Source183 datapoints were not Deleted successfully.");

  for(int i = 1; i <= dynlen(rack); i++) {
    if(0 != dpDelete(mmgGas + "Distribution_Rack" + rack[i], "fwAtlasGasRack"))
      error(mmgGas + "Distribution_Rack" + rack[i] + " datapoints were not Deleted successfully.");
    for(int chan = 1; chan <= 16 ; chan++) {
      if(0 != dpDelete(mmgGas + "Distribution_Rack" + rack[i] + "_Channel" + chan, "fwAtlasGasChannel"))
        error(mmgGas + "Distribution_Rack" + rack[i] + "_Channel" + chan + " datapoints were not Deleted successfully.");
    }
  }
  if(0 != dpDelete("template_fwAtlasGasRack", "fwAtlasGasRack"))
    error("template_fwAtlasGasRack datapoints were not Deleted successfully.");
  if(0 != dpDelete("template_fwAtlasGasChannel", "fwAtlasGasChannel"))
    error("template_fwAtlasGasChannel datapoints were not Deleted successfully.");

  if(0 != dpDelete(mmgGas + "Distribution", "fwAtlasGasDistribution"))
    error(mmgGas + "Distribution datapoints were not Deleted successfully.");
  if(0 != dpDelete("template_fwAtlasGasDistribution", "fwAtlasGasDistribution"))
    error("template_fwAtlasGasDistribution datapoints were not Deleted successfully.");

  if(0 != dpDelete(mmgGas + "GasSystem", "fwAtlasGasGasSystem"))
    error(mmgGas + "GasSystem datapoints were not Deleted successfully.");
  if(0 != dpDelete("template_fwAtlasGasGasSystem", "fwAtlasGasGasSystem"))
    error("template_fwAtlasGasGasSystem datapoints were not Deleted successfully.");

  if(0 != dpDelete(mmgGas + "Mixer", "fwAtlasGasMixer"))
    error(mmgGas + "Mixer datapoints were not Deleted successfully.");
  if(0 != dpDelete("template_fwAtlasGasMixer", "fwAtlasGasMixer"))
    error("template_fwAtlasGasMixer datapoints were not Deleted successfully.");

  if(0 != dpDelete(mmgGas + "PlcCounter", "fwAtlasGasPlcCounter"))
    error(mmgGas + "PlcCounter datapoints were not Deleted successfully.");
  if(0 != dpDelete("template_fwAtlasGasPlcCounter", "fwAtlasGasPlcCounter"))
    error("template_fwAtlasGasPlcCounter datapoints were not Deleted successfully.");
}

void mmgGas_alertsActivateAll() {

  string sys = mmgGas_getGasSystemName();
  dyn_string dptList = dpTypes("fwAtlasGas*");
  int pos1 = dynContains(dptList, "fwAtlasGasProjMonitor");
  int pos2 = dynContains(dptList, "fwAtlasGasChain");
  int pos3 = dynContains(dptList, "fwAtlasGasSource");
  dynRemove(dptList, pos1);
  dynRemove(dptList, pos2);
  dynRemove(dptList, pos3);
  dyn_string exceptionInfo;

  for (int i = 1; i <= dynlen(dptList); i++) {
    dyn_string dpeNames = fwAtlasGas_getDptDpes(dptList[i]);
    if (!dynlen(dpeNames)) {
      information("Data point type " + dptList[i] +
                  "seems to have no elements ....");
      continue;
    }

    for (int j = 1; j <= dynlen(dpeNames); j++) {

      dyn_string dpeList = dpNames(sys + "gas_MMG*." + dpeNames[j], dptList[i]);

      if(!dynlen(dpeList)) {
        information("There is no DP like gas_MMG*." + dpeNames[j] + " of DPT " + dptList[i]);
        break;
      }

      for (int k = 1; k <= dynlen(dpeList); k++) {

        fwAlertConfig_activate(dpSubStr(dpeList[k], DPSUB_DP_EL), exceptionInfo);
      }
    }
  }
}

void mmgGas_alertsDeactivateAll() {

  string sys = mmgGas_getGasSystemName();
  dyn_string dptList = dpTypes("fwAtlasGas*");
  int pos = dynContains(dptList, "fwAtlasGasProjMonitor");
  dynRemove(dptList, pos);
  dyn_string exceptionInfo;

  for (int i = 1; i <= dynlen(dptList); i++) {
    dyn_string dpeNames = fwAtlasGas_getDptDpes(dptList[i]);
    if (!dynlen(dpeNames)) {
      information("Data point type " + dptList[i] +
                  "seems to have no elements ....");
      continue;
    }


    for (int j = 1; j <= dynlen(dpeNames); j++) {

      dyn_string dpeList = dpNames(sys + "gas_MMG*." + dpeNames[j], dptList[i]);

      if(!dynlen(dpeList)) {
        information("There is no DP like gas_MMG*." + dpeNames[j] + " of DPT " + dptList[i]);
        break;
      }

      for (int k = 1; k <= dynlen(dpeList); k++) {

        fwAlertConfig_deactivate(dpSubStr(dpeList[k], DPSUB_DP_EL), exceptionInfo);
      }
    }
  }
}

void mmgGas_setUserBits() {

  string sys = mmgGas_getGasSystemName();
  dyn_string dptList = dpTypes("fwAtlasGas*");
  int pos = dynContains(dptList, "fwAtlasGasProjMonitor");
  dynRemove(dptList, pos);

  for (int i = 1; i <= dynlen(dptList); i++) {
    dyn_string dpeNames = fwAtlasGas_getDptDpes(dptList[i]);
    if (!dynlen(dpeNames)) {
      information("Data point type " + dptList[i] +
                  "seems to have no elements ....");
      continue;
    }


    for (int j = 1; j <= dynlen(dpeNames); j++) {

      dyn_string dpeList = dpNames(sys + "gas_MMG*." + dpeNames[j], dptList[i]);

      if(!dynlen(dpeList)) {
        information("There is no DP like gas_MMG*." + dpeNames[j] + " of DPT " + dptList[i]);
        break;
      }

      for (int k = 1; k <= dynlen(dpeList); k++) {

        dpSetWait(dpSubStr(dpeList[k], DPSUB_DP_EL) + ":_original.._userbit9", TRUE);
      }
    }
  }
}

void mmgGas_gasLossDpFunc() {

  dyn_string exceptionInfo;
  string sys = mmgGas_getGasSystemName();
  dyn_string channels = dpNames(sys + "gas_MMG*", "fwAtlasGasChannel");

  for(int i = 1; i <= dynlen(channels); i++) {

    fwDpFunction_setDpeConnection(channels[i] + ".UserDefined.GasLoss",
                                  makeDynString(channels[i] + ".InFlow:_online.._value",
                                                channels[i] + ".OutFlow:_online.._value"),
                                  makeDynString(),
                                  "p1 - p2", exceptionInfo);
  }

}

void mmgGas_gasLossAlerts() {

  dyn_string ranges = makeDynString("LOW_LOW", "LOW", "OK", "HIGH", "HIGH_HIGH");

  dyn_float leaks = makeDynFloat(-10, 0, 5, 10);
  dyn_string alert_classes = makeDynString("_fwErrorNack", "_fwWarningNack", "", "_fwWarningNack", "_fwErrorNack");

  dyn_string exceptionInfo;
  string sys = mmgGas_getGasSystemName();

  dyn_string dpes = dpNames(sys + "gas_MMG_*.UserDefined.GasLoss", "fwAtlasGasChannel");

  for(int i = 1; i <= dynlen(dpes); i++) {
    fwAlertConfig_set(dpes[i],
                        DPCONFIG_ALERT_NONBINARYSIGNAL,
                        ranges,
                        leaks,
                        alert_classes,
                        "", "", "", "", exceptionInfo, 0, 0, "", 1);
    fwAlertConfig_activate(dpes[i], exceptionInfo);
  }

  if(dynlen(exceptionInfo) < 1)
    information("Alarms for fwAtlasGasChannel -> GasLoss have been set.");
  else {
    error("Alarms for fwAtlasGasChannel -> GasLoss have not been set successfully. Errors:");
    DebugN(exceptionInfo);
  }
}

void mmgGas_gasLossArchive() {

  string sys = mmgGas_getGasSystemName();
  dyn_string exceptionInfo;
  string archiveClassName = "RDB-99) EVENT";
  int archiveType = DPATTR_ARCH_PROC_SIMPLESM;
  int smoothProcedure = DPATTR_TIME_AND_VALUE_REL_SMOOTH;
  float deadband = 3;
  float timeInterval = 3600;
  dyn_string channels = dpNames(sys + "gas_MMG_*.UserDefined.GasLoss", "fwAtlasGasChannel");

  for(int i = 1; i <= dynlen(channels); i++) {

    fwArchive_config(channels[i], archiveClassName, archiveType, smoothProcedure,
                         deadband, timeInterval, exceptionInfo);
    fwArchive_start(channels[i], exceptionInfo);
  }
}

void mmgGas_deactivateNonUsedAlerts() {

  string sys = mmgGas_getGasSystemName();
  dyn_string alerts = makeDynString(sys + "gas_MMG_Distribution_Rack62.ChPressure",
                                    sys + "gas_MMG_Distribution_Rack62.RegPressure",
                                    sys + "gas_MMG_Distribution_Rack61.ChPressure",
                                    sys + "gas_MMG_Distribution_Rack61.RegPressure",
                                    sys + "gas_MMG_Distribution.TotalInputFlow",
                                    sys + "gas_MMG_Mixer.TotalVol",
                                    sys + "gas_MMG_Mixer.Line1TotRunningTime",
                                    sys + "gas_MMG_Mixer.Line2TotRunningTime",
                                    sys + "gas_MMG_Mixer.Line3TotRunningTime");

  dyn_string pres_alerts = dpNames(sys + "gas_MMG_*.Pressure", "fwAtlasGasChannel");
  dyn_string exceptionInfo;

  for(int i = 1; i <= dynlen(alerts); i++) {

    fwAlertConfig_deactivate(alerts[i], exceptionInfo);
  }
  for(int j = 1; j <= dynlen(pres_alerts); j++) {

    fwAlertConfig_deactivate(pres_alerts[j], exceptionInfo);
  }
}
