//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)

  "mmgGas_FSMHandling.ctl" is a library to be used to handle FSM operations."
*/


void mmgGas_createFsmTree() {

  // createNode: parent, node name, object type,
  //             label, panel ref, type (LU = 0, CU = 1, DU = 2)

  fwFsmAtlas_createNode("FSM", "MMG_GAS_SYSTEM", "ATLAS_GAS_SYSTEM",
                        "MMG_GAS_SYSTEM", "mmgGas/MMG_GAS_SYSTEM", 1);

    fwFsmAtlas_createNode("MMG_GAS_SYSTEM", "gas_MMG_Mixer", "fwAtlasGasMixerDU",
                          "MIXER", "mmgGas/MMG_GAS_MIXER", 2);
    fwFsmAtlas_createNode("MMG_GAS_SYSTEM", "MMG_GAS_DISTRIBUTION", "ATLAS_GAS_DISTRIBUTION",
                          "DISTRIBUTION", "mmgGas/MMG_GAS_DISTRIBUTION", 1);

      fwFsmAtlas_createNode("MMG_GAS_DISTRIBUTION", "gas_MMG_Distribution", "fwAtlasGasDistributionDU",
                            "Distribution Module", "mmgGas/MMG_GAS_DISTRIBUTION", 2);
      fwFsmAtlas_createNode("MMG_GAS_DISTRIBUTION", "MMG_GAS_RACK61", "ATLAS_GAS_RACK",
                            "RACK 61", "mmgGas/MMG_GAS_RACK", 0);

        fwFsmAtlas_createNode("MMG_GAS_RACK61", "gas_MMG_Distribution_Rack61", "fwAtlasGasRackDU",
                              "Rack Control", "mmgGas/MMG_GAS_RACK", 2);
      for(int c = 1; c <= 16; c++) {
        fwFsmAtlas_createNode("MMG_GAS_RACK61", "gas_MMG_Distribution_Rack61_Channel" + c, "fwAtlasGasChannelDU",
                              "CHANNEL " + c, "mmgGas/MMG_GAS_CHANNEL", 2);
      }

      fwFsmAtlas_createNode("MMG_GAS_DISTRIBUTION", "MMG_GAS_RACK62", "ATLAS_GAS_RACK",
                            "RACK 62", "mmgGas/MMG_GAS_RACK", 0);

        fwFsmAtlas_createNode("MMG_GAS_RACK62", "gas_MMG_Distribution_Rack62", "fwAtlasGasRackDU",
                              "Rack Control", "mmgGas/MMG_GAS_RACK", 2);

      for(int c = 1; c <= 16; c++) {
        fwFsmAtlas_createNode("MMG_GAS_RACK62", "gas_MMG_Distribution_Rack62_Channel" + c, "fwAtlasGasChannelDU",
                              "CHANNEL " + c, "mmgGas/MMG_GAS_CHANNEL", 2);
      }
}
